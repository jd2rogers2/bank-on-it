import os
from psycopg_pool import ConnectionPool
from pydantic import BaseModel

pool = ConnectionPool(conninfo=os.environ["DATABASE_URL"])


class UserIn(BaseModel):
    username: str
    password: str

class UserOut(BaseModel):
    id: int
    username: str

class UserWithPw(BaseModel):
    id: int
    username: str
    password: str

class UserQueries:
    def get_by_username(self, username: str) -> UserWithPw:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM users
                    WHERE username = %s
                    """,
                    [username]
                )
                result = cur.fetchone()
                return UserWithPw(
                    id=result[0],
                    username=result[1],
                    password=result[2],
                )

    def get_by_id(self, id: int) -> UserWithPw:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    SELECT
                        *
                    FROM users
                    WHERE id = %s
                    """,
                    [id]
                )
                result = cur.fetchone()
                return UserWithPw(
                    id=result[0],
                    username=result[1],
                    password=result[2],
                )

    def create_user(self, username: str, hashed_password: str) -> UserWithPw:
        with pool.connection() as conn:
            with conn.cursor() as cur:
                cur.execute("""
                    INSERT INTO users (
                        username,
                        password
                    ) VALUES (
                        %s, %s
                    )
                    RETURNING *;
                    """,
                    [
                        username,
                        hashed_password,
                    ]
                )
                result = cur.fetchone()
                return UserWithPw(
                    id=result[0],
                    username=result[1],
                    password=result[2],
                )

