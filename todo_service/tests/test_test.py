from main import app
from unittest.mock import patch
from fastapi.testclient import TestClient

from queries.todo_queries import TodoOut
from psycopg_pool import ConnectionPool

client = TestClient(app)

class MockCursor:
    def execute(sql):
        pass

    def fetchone():
        return [1, 'action', 'description', 1]

class MockConnection:
    def cursor():
        return MockCursor()
    
    def __enter__():
        pass

def test_test():
    expected = TodoOut(id=1, action='action', description='description', value=1)
    
    with patch.object(ConnectionPool, 'connection', return_value=MockConnection()):
        response = client.get('/api/todos/1')
        
        assert response.status_code == 200
