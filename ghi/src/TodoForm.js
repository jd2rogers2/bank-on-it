import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import { useAuthContext } from "./authService";

function TodoForm() {
  const [formData, setFormData] = useState({});
  const { user } = useAuthContext();
  const navigate = useNavigate();

  useEffect(() => {
    if (!user?.id) {
      navigate("/signin");
    }
  }, [user]);

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]:
        e.name === "value" ? Number(e.target.value) : e.target.value,
    });
  };

  const handleFormSubmit = async (e) => {
    await fetch(`${process.env.REACT_APP_TODO_SERVICE_API_HOST}/api/todos`, {
      method: "post",
      credentials: "include",
      body: JSON.stringify(formData),
      headers: {
        "Content-Type": "application/json",
      },
    });
    navigate("/my-list");
  };

  const { action = "", description = "", value = "" } = formData;
  return (
    <div>
      <ul>
        <li>
          action{" "}
          <input
            type="text"
            name="action"
            value={action}
            onChange={handleFormChange}
          />
        </li>
        <li>
          description{" "}
          <input
            type="text"
            name="description"
            value={description}
            onChange={handleFormChange}
          />
        </li>
        <li>
          value{" "}
          <input
            type="text"
            name="value"
            value={value}
            onChange={handleFormChange}
          />
        </li>
      </ul>
      <button onClick={handleFormSubmit}>create todo</button>
    </div>
  );
}

export default TodoForm;
