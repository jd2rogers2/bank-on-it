import React, { createContext, useContext, useEffect, useState } from "react";

const baseUrl = process.env.REACT_APP_TODO_SERVICE_API_HOST;

export const AuthContext = createContext({
  user: undefined,
  setUser: () => {},
  isLoading: true,
  setIsLoading: () => {},
});

export const AuthProvider = (props) => {
  const [user, setUser] = useState(); // undefined for incomplete fetch then null or user object
  const [isLoading, setIsLoading] = useState(true);
  const { children } = props;

  const fetchUser = async () => {
    const url = `${baseUrl}/current`;
    const res = await fetch(url, {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (res.ok) {
      const newUser = await res.json();
      setUser(newUser);
    } else {
      setUser(null);
      console.error("fetch current user failed");
    }
    setIsLoading(false);
  };

  useEffect(() => {
    if (user === undefined) {
      fetchUser();
    }
  }, [user]);

  return (
    <AuthContext.Provider value={{ user, setUser, isLoading, setIsLoading }}>
      {isLoading ? <div>loading...</div> : children}
    </AuthContext.Provider>
  );
};

export const useAuthContext = () => useContext(AuthContext);

const useAuthService = () => {
  const { user, setUser, isLoading } = useAuthContext();

  const signup = async (userData) => {
    const url = `${baseUrl}/signup`;
    const res = await fetch(url, {
      method: "POST",
      body: JSON.stringify(userData),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!res.ok) {
      console.error("sign up failed");
      return false;
    }
    const newUser = await res.json();
    setUser(newUser);
    return true;
  };

  const signin = async (userData) => {
    const url = `${baseUrl}/signin`;
    const res = await fetch(url, {
      method: "POST",
      body: JSON.stringify(userData),
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
      },
    });
    if (!res.ok) {
      console.error("sign in failed");
      return false;
    }
    const newUser = await res.json();
    setUser(newUser);
    return true;
  };

  const signout = async () => {
    const url = `${baseUrl}/signout`;
    const res = await fetch(url, {
      method: "DELETE",
      credentials: "include",
    });
    if (!res.ok) {
      console.error("sign out failed, manually deleting cookie");
      return false;
    }
    setUser(null);
    return true;
  };

  return {
    signup,
    signin,
    signout,
    user,
    isLoading,
  };
};

export default useAuthService;
