import { useEffect, useState } from "react";
import useAuthService from "./authService";
import { useNavigate, useLocation } from "react-router-dom";

function Auth() {
  const [formData, setFormData] = useState({});
  const { user, signin, signup } = useAuthService();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    if (user?.id) {
      navigate("/my-list");
    }
  }, [user]);

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const isSignIn = location.pathname.includes("signin");

  const handleFormSubmit = async (e) => {
    let success = false;
    if (isSignIn) {
      success = await signin(formData);
    } else {
      success = await signup(formData);
    }
    if (success) {
      navigate("/my-list");
    }
  };

  const { username = "", password = "" } = formData;
  return user?.id ? null : (
    <div>
      <ul>
        <li>
          username{" "}
          <input
            type="text"
            name="username"
            value={username}
            onChange={handleFormChange}
          />
        </li>
        <li>
          password{" "}
          <input
            type="text"
            name="password"
            value={password}
            onChange={handleFormChange}
          />
        </li>
      </ul>
      <button onClick={handleFormSubmit}>
        {isSignIn ? "sign in" : "sign up"}
      </button>
    </div>
  );
}

export default Auth;
